#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <stdint.h>

#define SIZE 32
#define BS 8

int64_t N;
//Size M = Size A * 2 = Size B * 2
__global__ void kernelM1(int64_t *A, int64_t *B, int64_t *M, int64_t size) {
    //Thread identifiers
    int64_t ty = threadIdx.y;
    int64_t tx = threadIdx.x;
    int64_t row = blockIdx.y * SIZE + ty;
    int64_t col = blockIdx.x * SIZE + tx;

    //Pointers to subMatrix
    int newSize = size >> 1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;

    __shared__ int64_t tmpA[SIZE][SIZE];
    __shared__ int64_t tmpB[SIZE][SIZE];

    int64_t tmp = 0;
    int64_t lim = (SIZE > newSize) ? newSize : SIZE;

    for (int m = 0; m < newSize; m += SIZE) {
	//Add before mult
	tmpA[ty][tx] = A[row*size + m + tx] + A[p22 + (row * size) + m + tx]; //A11 + A22
	tmpB[ty][tx] = B[(m+ty)*size + col] + B[p22 + (m+ty)*size + col]; //B11 + B22
	__syncthreads();

	//M1 = (A11 + A22) * (B11 + B22)
	for (int k = 0; k < lim; ++k) {
	    tmp += tmpA[ty][k] * tmpB[k][tx];
	}
	__syncthreads();
    }
    M[row * newSize + col] = tmp;
}

//Size M = Size A * 2 = Size B * 2
__global__ void kernelM2(int64_t *A, int64_t *B, int64_t *M, int64_t size) {
    //Thread identifiers
    int64_t ty = threadIdx.y;
    int64_t tx = threadIdx.x;
    int64_t row = blockIdx.y * SIZE + ty;
    int64_t col = blockIdx.x * SIZE + tx;

    //Pointers to subMatrix
    int newSize = size >> 1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;

    __shared__ int64_t tmpA[SIZE][SIZE];
    __shared__ int64_t tmpB[SIZE][SIZE];

    int64_t tmp = 0;
    int64_t lim = (SIZE > newSize) ? newSize : SIZE;

    for (int m = 0; m < newSize; m += SIZE) {
	//Add before mult
	tmpA[ty][tx] = A[p21 + row*size + m + tx] + A[p22 + (row * size) + m + tx]; //A21 + A22
	tmpB[ty][tx] = B[(m+ty)*size + col]; //B11
	__syncthreads();

	//M2 = (A21 + A22) * B11
	for (int k = 0; k < lim; ++k) {
	    tmp += tmpA[ty][k] * tmpB[k][tx];
	}
	__syncthreads();
    }
    M[row * newSize + col] = tmp;
}

//Size M = Size A * 2 = Size B * 2
__global__ void kernelM3(int64_t *A, int64_t *B, int64_t *M, int64_t size) {
    //Thread identifiers
    int64_t ty = threadIdx.y;
    int64_t tx = threadIdx.x;
    int64_t row = blockIdx.y * SIZE + ty;
    int64_t col = blockIdx.x * SIZE + tx;

    //Pointers to subMatrix
    int newSize = size >> 1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;

    __shared__ int64_t tmpA[SIZE][SIZE];
    __shared__ int64_t tmpB[SIZE][SIZE];

    int64_t tmp = 0;
    int64_t lim = (SIZE > newSize) ? newSize : SIZE;

    for (int m = 0; m < newSize; m += SIZE) {
	//Add before mult
	tmpA[ty][tx] = A[row*size + m + tx]; //A11
	tmpB[ty][tx] = B[p12 + (m+ty)*size + col] - B[p22 + (m+ty)*size + col]; //B12 - B22
	__syncthreads();

	//M3 = A11 * (B12 - B22)
	for (int k = 0; k < lim; ++k) {
	    tmp += tmpA[ty][k] * tmpB[k][tx];
	}
	__syncthreads();
    }
    M[row * newSize + col] = tmp;
}

//Size M = Size A * 2 = Size B * 2
__global__ void kernelM4(int64_t *A, int64_t *B, int64_t *M, int64_t size) {
    //Thread identifiers
    int64_t ty = threadIdx.y;
    int64_t tx = threadIdx.x;
    int64_t row = blockIdx.y * SIZE + ty;
    int64_t col = blockIdx.x * SIZE + tx;

    //Pointers to subMatrix
    int newSize = size >> 1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;

    __shared__ int64_t tmpA[SIZE][SIZE];
    __shared__ int64_t tmpB[SIZE][SIZE];

    int64_t tmp = 0;
    int64_t lim = (SIZE > newSize) ? newSize : SIZE;

    for (int m = 0; m < newSize; m += SIZE) {
	//Add before mult
	tmpA[ty][tx] = A[p22 + row*size + m + tx]; //A22
	tmpB[ty][tx] = B[p21 + (m+ty)*size + col] - B[(m+ty)*size + col]; //B21 - B11
	__syncthreads();

	//M4 = A11 * (B12 - B22)
	for (int k = 0; k < lim; ++k) {
	    tmp += tmpA[ty][k] * tmpB[k][tx];
	}
	__syncthreads();
    }
    M[row * newSize + col] = tmp;
}

//Size M = Size A * 2 = Size B * 2
__global__ void kernelM5(int64_t *A, int64_t *B, int64_t *M, int64_t size) {
    //Thread identifiers
    int64_t ty = threadIdx.y;
    int64_t tx = threadIdx.x;
    int64_t row = blockIdx.y * SIZE + ty;
    int64_t col = blockIdx.x * SIZE + tx;

    //Pointers to subMatrix
    int newSize = size >> 1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;

    __shared__ int64_t tmpA[SIZE][SIZE];
    __shared__ int64_t tmpB[SIZE][SIZE];

    int64_t tmp = 0;
    int64_t lim = (SIZE > newSize) ? newSize : SIZE;

    for (int m = 0; m < newSize; m += SIZE) {
	//Add before mult
	tmpA[ty][tx] = A[row*size + m + tx] + A[p12 + row*size + m + tx]; //A11 + A12
	tmpB[ty][tx] = B[p22 + (m+ty)*size + col]; //B22
	__syncthreads();

	//M5 = (A11 + A12) * (B22)
	for (int k = 0; k < lim; ++k) {
	    tmp += tmpA[ty][k] * tmpB[k][tx];
	}
	__syncthreads();
    }
    M[row * newSize + col] = tmp;
}

//Size M = Size A * 2 = Size B * 2
__global__ void kernelM6(int64_t *A, int64_t *B, int64_t *M, int64_t size) {
    //Thread identifiers
    int64_t ty = threadIdx.y;
    int64_t tx = threadIdx.x;
    int64_t row = blockIdx.y * SIZE + ty;
    int64_t col = blockIdx.x * SIZE + tx;

    //Pointers to subMatrix
    int newSize = size >> 1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;

    __shared__ int64_t tmpA[SIZE][SIZE];
    __shared__ int64_t tmpB[SIZE][SIZE];

    int64_t tmp = 0;
    int64_t lim = (SIZE > newSize) ? newSize : SIZE;

    for (int m = 0; m < newSize; m += SIZE) {
	//Add before mult
	tmpA[ty][tx] = A[p21 + row*size + m + tx] - A[row*size + m + tx]; //A21 + A11
	tmpB[ty][tx] = B[(m+ty)*size + col] + B[p12 + (m+ty)*size + col]; //B11 + B12
	__syncthreads();

	//M6 = (A21 - A11) * (B11 + B12)
	for (int k = 0; k < lim; ++k) {
	    tmp += tmpA[ty][k] * tmpB[k][tx];
	}
	__syncthreads();
    }
    M[row * newSize + col] = tmp;
}

//Size M = Size A * 2 = Size B * 2
__global__ void kernelM7(int64_t *A, int64_t *B, int64_t *M, int64_t size) {
    //Thread identifiers
    int64_t ty = threadIdx.y;
    int64_t tx = threadIdx.x;
    int64_t row = blockIdx.y * SIZE + ty;
    int64_t col = blockIdx.x * SIZE + tx;

    //Pointers to subMatrix
    int newSize = size >> 1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;

    __shared__ int64_t tmpA[SIZE][SIZE];
    __shared__ int64_t tmpB[SIZE][SIZE];

    int64_t tmp = 0;
    int64_t lim = (SIZE > newSize) ? newSize : SIZE;

    for (int m = 0; m < newSize; m += SIZE) {
	//Add before mult
	tmpA[ty][tx] = A[p12 + row*size + m + tx] - A[p22 + row*size + m + tx]; //A12 - A22
	tmpB[ty][tx] = B[p21 + (m+ty)*size + col] + B[p22 + (m+ty)*size + col]; //B21 + B22
	__syncthreads();

	//M6 = (A12 - A22) * (B21 + B22)
	for (int k = 0; k < lim; ++k) {
	    tmp += tmpA[ty][k] * tmpB[k][tx];
	}
	__syncthreads();
    }
    M[row * newSize + col] = tmp;
}

__global__ void kernelResult(int64_t *C, int64_t size, int64_t *M1, int64_t *M2, int64_t *M3, int64_t *M4, int64_t *M5, int64_t *M6, int64_t *M7) {
    int64_t row = blockIdx.y * blockDim.y + threadIdx.y;
    int64_t col = blockIdx.x * blockDim.x + threadIdx.x;

    //Pointers to subMatrix
    int newSize = size >> 1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;
    int64_t pos = row*newSize + col;

    if (row < newSize && col < newSize) {
	C[row*size + col] = M1[pos] + M4[pos] - M5[pos] + M7[pos];
	C[p12 + row*size + col] = M3[pos] + M5[pos];
	C[p21 + row*size + col] = M2[pos] + M4[pos];
	C[p22 + row*size + col] = M1[pos] - M2[pos] + M3[pos] + M6[pos];
    }
}

bool check(int64_t *A, int64_t *B, const int64_t dim) {
    int64_t i;
    for (i = 0; i < (dim*dim); ++i) {
	if (A[i] != B[i]) return false;
    }
    return true;
}

void mult(int64_t *A, int64_t *B, int64_t *C, const int64_t dim) {
    int64_t i, j, k, ii, jj;
    int64_t tmp;
    for (ii = 0; ii < dim; ii+=BS) {
	for (jj = 0; jj < dim; jj+=BS) {
	    for (i = 0; i < dim; ++i) {
		for (j = ii; j < (ii+BS); ++j) {
		    tmp = 0;
		    for (k = jj; k < (jj+BS); ++k) {
			tmp += A[i*dim + k]*B[k*dim + j];
		    }
		    C[i*dim + j] += tmp;
		}
	    }
	}
    }
    /*for (i = 0; i < dim; ++i) {
	for (j = 0; j < dim; ++j) {
	    tmp = 0;
	    for (k = 0; k < dim; ++k) {
		tmp += A[i*dim + k] * B[k*dim + j];
	    }
	    C[i*dim + j] = tmp;
	}
    }*/
}

void print(int64_t *matrix, const int64_t dim) {
    printf("\n");
    int64_t i, j, k, tmp, tmp1, tmp2;
    
    tmp1 = matrix[0];
    for (i = 1; i < dim*dim; ++i) {
	if (matrix[i] > tmp1) tmp1 = matrix[i];
    }
    tmp2 = snprintf(NULL, 0, "%ld", tmp1);

    for (i = 0; i < dim; ++i) {
	for (j = 0; j < dim; ++j) {
	    tmp = matrix[i*dim + j];
	    tmp1 = tmp2 - snprintf(NULL, 0, "%ld", tmp) + 1;
	    printf("%ld", tmp);
	    for (k = 0; k < tmp1; ++k) printf(" ");
	    if ((j+1) % 16 == 0) {
		printf("\n");
	    }
	}
	printf("\n\n");
    }
}

void printSubMatrix(int64_t *matrix, const int64_t dim) {
    printf("\n");
    int64_t i, j, k, tmp, tmp1, tmp2;
    
    tmp1 = matrix[0];
    for (i = 1; i < dim*dim; ++i) {
	if (matrix[i] > tmp1) tmp1 = matrix[i];
    }
    tmp2 = snprintf(NULL, 0, "%ld", tmp1);
   
    printf("submatrix 11\n"); 
    for (i = 0; i < (dim >> 1); ++i) {
	for (j = 0; j < (dim >> 1); ++j) {
	    tmp = matrix[i*dim + j];
	    tmp1 = tmp2 - snprintf(NULL, 0, "%ld", tmp) + 1;
	    printf("%ld", tmp);
	    for (k = 0; k < tmp1; ++k) printf(" ");
	    if ((j+1) % 16 == 0) {
		printf("\n");
	    }
	}
	printf("\n\n");
    }
    
    printf("submatrix 12\n");
    for (i = 0; i < (dim >> 1); ++i) {
	for (j = 0; j < (dim >> 1); ++j) {
	    tmp = matrix[i*dim + j + (dim>>1)];
	    tmp1 = tmp2 - snprintf(NULL, 0, "%ld", tmp) + 1;
	    printf("%ld", tmp);
	    for (k = 0; k < tmp1; ++k) printf(" ");
	    if ((j+1) % 16 == 0) {
		printf("\n");
	    }
	}
	printf("\n\n");
    }

    printf("submatrix 21\n");
    for (i = 0; i < (dim >> 1); ++i) {
	for (j = 0; j < (dim >> 1); ++j) {
	    tmp = matrix[(i+(dim>>1))*dim + j];
	    tmp1 = tmp2 - snprintf(NULL, 0, "%ld", tmp) + 1;
	    printf("%ld", tmp);
	    for (k = 0; k < tmp1; ++k) printf(" ");
	    if ((j+1) % 16 == 0) {
		printf("\n");
	    }
	}
	printf("\n\n");
    }

    printf("submatrix 22\n");
    for (i = 0; i < (dim >> 1); ++i) {
	for (j = 0; j < (dim >> 1); ++j) {
	    tmp = matrix[(i+(dim>>1))*dim + j + (dim>>1)];
	    tmp1 = tmp2 - snprintf(NULL, 0, "%ld", tmp) + 1;
	    printf("%ld", tmp);
	    for (k = 0; k < tmp1; ++k) printf(" ");
	    if ((j+1) % 16 == 0) {
		printf("\n");
	    }
	}
	printf("\n\n");
    }
}

long getTime(struct timeval *t1, struct timeval *t2) {
    long elapsed = (t2->tv_sec - t1->tv_sec) * 1000L;
    elapsed += (t2->tv_usec - t1->tv_usec) / 1000L;
    return elapsed;
}

void checkError(cudaError_t cudaError, char *s, int64_t line) {
    if (cudaError != cudaSuccess) {
	printf("%s failed with error code %d at line %d\n", s, cudaError, line);
	printf("%s\n", cudaGetErrorString(cudaError));
	abort();
    }
}

void init(int64_t *M, const int64_t dim) {
    int i;
    for (i = 0; i < (dim*dim); ++i) {
	M[i] = 1;
    }
}

int main(int argc, char *argv[]) {
    char testing;
    if (argc > 2) {
	N = atoi(argv[1]);
	testing = *argv[2];
    }
    else N = 512;
    
    float TiempoTotal, TiempoKernel;
    cudaEvent_t E0, E1, E2, E3;
    int64_t nBlocks, nThreads;

    //Matrix
    int64_t *h_A, *h_B, *h_C;
    int64_t *d_A, *d_B, *d_C;
    int64_t *M1, *M2, *M3, *M4, *M5, *M6, *M7, *tmpA, *tmpB;
    int64_t *tmpC, *tmpD, *tmpE, *tmpF, *tmpG, *tmpH, *tmpI, *tmpJ;
    int64_t *h_M1, *h_M2, *h_M3, *h_M4, *h_M5, *h_M6, *h_M7;

    nThreads = 32;
    nBlocks = ((N>>1)+nThreads-1)/nThreads;
    int64_t numBytes = N*N*sizeof(int64_t);
    int64_t numBytes_tmp = (N/2)*(N/2)*sizeof(int64_t);
    
    dim3 dimGrid(nBlocks, nBlocks, 1);
    dim3 dimBlock(nThreads, nThreads, 1);
   
    cudaEventCreate(&E0);
    cudaEventCreate(&E1);
    cudaEventCreate(&E2);
    cudaEventCreate(&E3);
     
    //host
    //h_A = (int64_t *) malloc(N*N*sizeof(int64_t));
    //h_B = (int64_t *) malloc(N*N*sizeof(int64_t));
    //h_C = (int64_t *) malloc(N*N*sizeof(int64_t));
    //h_M1 = (int64_t *) malloc(numBytes_tmp);

    cudaMallocHost((int64_t**)&h_A,N*N*sizeof(int64_t));
    cudaMallocHost((int64_t**)&h_B,N*N*sizeof(int64_t));
    cudaMallocHost((int64_t**)&h_C,N*N*sizeof(int64_t));
    cudaMallocHost((int64_t**)&h_M1,numBytes_tmp);

    //device
    cudaMalloc((int64_t **)&d_A, numBytes);
    checkError(cudaGetLastError(), "cudaMalloc 1", __LINE__);
    cudaMalloc((int64_t **)&d_B, numBytes);
    checkError(cudaGetLastError(), "cudaMalloc 2", __LINE__);
    cudaMalloc((int64_t **)&d_C, numBytes);
    checkError(cudaGetLastError(), "cudaMalloc 3", __LINE__);
    cudaMalloc((int64_t **)&M1, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 4", __LINE__);
    cudaMalloc((int64_t **)&M2, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 5", __LINE__);
    cudaMalloc((int64_t **)&M3, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 6", __LINE__);
    cudaMalloc((int64_t **)&M4, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 7", __LINE__);
    cudaMalloc((int64_t **)&M5, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 8", __LINE__);
    cudaMalloc((int64_t **)&M6, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 9", __LINE__);
    cudaMalloc((int64_t **)&M7, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 10", __LINE__);
     
    //init
    init(h_A, N);
    init(h_B, N);
    
    cudaEventRecord(E0, 0);
    cudaEventSynchronize(E0);

    //Copy to device
    cudaMemcpy(d_A, h_A, numBytes, cudaMemcpyHostToDevice); 
    checkError(cudaGetLastError(), "cudaMemcpy 1", __LINE__);
    cudaMemcpy(d_B, h_B, numBytes, cudaMemcpyHostToDevice);
    checkError(cudaGetLastError(), "cudaMemcpy 2", __LINE__);

    cudaEventRecord(E1, 0);
    cudaEventSynchronize(E1);

    kernelM1<<<dimGrid, dimBlock>>>(d_A, d_B, M1, N);
    kernelM2<<<dimGrid, dimBlock>>>(d_A, d_B, M2, N);
    kernelM3<<<dimGrid, dimBlock>>>(d_A, d_B, M3, N);
    kernelM4<<<dimGrid, dimBlock>>>(d_A, d_B, M4, N);
    kernelM5<<<dimGrid, dimBlock>>>(d_A, d_B, M5, N);
    kernelM6<<<dimGrid, dimBlock>>>(d_A, d_B, M6, N);
    kernelM7<<<dimGrid, dimBlock>>>(d_A, d_B, M7, N);
    kernelResult<<<dimGrid, dimBlock>>>(d_C, N, M1, M2, M3, M4, M5, M6, M7);
    checkError(cudaGetLastError(), "kernel execution", __LINE__);

    cudaEventRecord(E2, 0);
    cudaEventSynchronize(E2);
        
    cudaMemcpy(h_C, d_C, numBytes, cudaMemcpyDeviceToHost);
    checkError(cudaGetLastError(), "cudaMemcpy result", __LINE__);

    //Free device memory
    cudaFree(d_A);
    cudaFree(d_B);
    cudaFree(d_C);
    cudaFree(M1);
    cudaFree(M2);
    cudaFree(M3);
    cudaFree(M4);
    cudaFree(M5);
    cudaFree(M6);

    cudaEventRecord(E3, 0);
    cudaEventSynchronize(E3);

    //Check result
    int64_t *h_D = (int64_t *) malloc(N*N*sizeof(int64_t));
    if(testing == 'Y'){	
	mult(h_A, h_B, h_D, N);
	bool result = check(h_D, h_C, N);
	if (result) {
	    printf("Correct result\n");
	}
	else {
	    printf("Wrong result\n");
	}
    }
    cudaEventElapsedTime(&TiempoTotal,  E0, E3);
    cudaEventElapsedTime(&TiempoKernel, E1, E2);
    printf("\nSTRASSEN 1 GPU\n");
    printf("Dimensiones: %dx%d\n", N, N);
    printf("nThreads: %dx%d (%d)\n", nThreads, nThreads, nThreads * nThreads);
    printf("nBlocks: %dx%d (%d)\n", nBlocks, nBlocks, nBlocks*nBlocks);
    /*if (PINNED) printf("Usando Pinned Memory\n");
    else printf("NO usa Pinned Memory\n");*/
    printf("Tiempo Global: %4.6f milseg\n", TiempoTotal);
    printf("Tiempo Kernel: %4.6f milseg\n", TiempoKernel);

    //Free host memory
    free(h_A); 
    free(h_B);
    free(h_C);
    free(h_D);
}
