#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define threshold 4

int N;

void init(int *matrix, const int dim) {
    int i;
    for (i = 0; i < dim*dim; ++i) {
	matrix[i] = i;
    }
}

void check(int *A, int *B, const int dim) {
    int i;
    for (i = 0; i < dim*dim; ++i) {
	if (A[i] != B[i]) {
	    printf("Wrong result at pos %d %d\n", i/dim, i%dim);
	    exit(0);
	}
    }
}

void print(int *matrix, const int dim) {
    printf("\n");
    int i, j, k, tmp, tmp1, tmp2;
    
    tmp1 = matrix[0];
    for (i = 1; i < dim*dim; ++i) {
	if (matrix[i] > tmp1) tmp1 = matrix[i];
    }
    tmp2 = snprintf(NULL, 0, "%d", tmp1);

    for (i = 0; i < dim; ++i) {
	for (j = 0; j < dim; ++j) {
	    tmp = matrix[i*dim + j];
	    tmp1 = tmp2 - snprintf(NULL, 0, "%d", tmp) + 1;
	    printf("%d", tmp);
	    for (k = 0; k < tmp1; ++k) printf(" ");
	}
	printf("\n");
    }
}

void multiplicate(int *A, int *B, int *C, int dim) {
    int i, j, k, tmp;
    for (i = 0; i < dim; ++i) {
	for (j = 0; j < dim; ++j) {
	    tmp = 0;
	    for (k = 0; k < dim; ++k) {
		tmp += A[i*dim + k]*B[k*dim + j];
	    }
	    C[i*dim + j] = tmp;
	}
    }
}

void sum(int *A, int *B, int *C, const int dim, const int dimC) {
    int i,j;
    for (i = 0; i < dim; ++i) {
	for (j = 0; j < dim; ++j) {
	    C[i*dimC + j] = A[i*dim + j] + B[i*dim + j];
	}
    }
}

void sub(int *A, int *B, int *C, const int dim, const int dimC) {
    int i, j;
    for (i = 0; i < dim; ++i) {
	for (j = 0; j < dim; ++j) {
	    C[i*dimC + j] = A[i*dim + j] - B[i*dim + j];
	}
    }
}

void strassen(int *A, int *B, int *C, int size) {
    if (size <= (N>>1)) {
	multiplicate(A, B, C, size);	
    }
    else {
	int newSize = size >> 1, mSize = newSize*newSize, i, j;
	int a11[mSize], a12[mSize], a21[mSize], a22[mSize];
	int b11[mSize], b12[mSize], b21[mSize], b22[mSize];
	int M1[mSize], M2[mSize], M3[mSize], M4[mSize], M5[mSize], M6[mSize], M7[mSize];
	int tmpA[mSize], tmpB[mSize];

	//Initialize 4 submatrix of A and B	
	for (i = 0; i < newSize; ++i) {
	    for (j = 0; j < newSize; ++j) {
		a11[i*newSize + j] = A[i*size + j]; 
		a12[i*newSize + j] = A[i*size + j + newSize];
		a21[i*newSize + j] = A[(i+newSize)*size + j];
		a22[i*newSize + j] = A[(i+newSize)*size + j + newSize];

		b11[i*newSize + j] = B[i*size + j];
		b12[i*newSize + j] = B[i*size + j + newSize];
		b21[i*newSize + j] = B[(i+newSize)*size + j];
		b22[i*newSize + j] = B[(i+newSize)*size + j + newSize];
	    }
	}
	
	//M1 = (A11 + A22) * (B11 + B22)
	sum((int *) &a11[0], (int *) &a22[0], (int *) &tmpA[0], newSize, newSize);
	sum((int *) &b11[0], (int *) &b22[0], (int *) &tmpB[0], newSize, newSize);
	strassen((int *) &tmpA[0], (int *) &tmpB[0], (int *) &M1[0], newSize);

	//M2 = (A21 + A22) * B11
	sum((int *) &a21[0], (int *) &a22[0], (int *) &tmpA[0], newSize, newSize);
	strassen((int *) &tmpA[0], (int *) &b11[0], (int *) &M2[0], newSize);
	
	//M3 = A11 * (B12 - B22)
	sub((int *) &b12[0], (int *) &b22[0], (int *) &tmpB[0], newSize, newSize);
	strassen((int *) &a11[0], (int *) &tmpB[0], (int *) &M3[0], newSize);

	//M4 = A22 * (B21 - B11)
	sub((int *) &b21[0], (int *) &b11[0], (int *) &tmpB[0], newSize, newSize);
	strassen((int *) &a22[0], (int *) &tmpB[0], (int *) &M4[0], newSize);

	//M5 = (A11 + A12) * B22
	sum((int *) &a11[0], (int *) &a12[0], (int *) &tmpA[0], newSize, newSize);
	strassen((int *) &tmpA[0], (int *) &b22[0], (int *) &M5[0], newSize);

	//M6 = (A21 - A11) * (B11 + B12)
	sub((int *) &a21[0], (int *) &a11[0], (int *) &tmpA[0], newSize, newSize);
	sum((int *) &b11[0], (int *) &b12[0], (int *) &tmpB[0], newSize, newSize);
	strassen((int *) &tmpA[0], (int *) &tmpB[0], (int *) &M6[0], newSize);
	
	//M7 = (A12 - A22) * (B21 + B22)
	sub((int *) &a12[0], (int *) &a22[0], (int *) &tmpA[0], newSize, newSize);
	sum((int *) &b21[0], (int *) &b22[0], (int *) &tmpB[0], newSize, newSize);
	strassen((int *) &tmpA[0], (int *) &tmpB[0], (int *) &M7[0], newSize);
	
	//Start positions of submatrix
	int p11 = 0, p12 = newSize, p21 = newSize*size, p22 = newSize*size + newSize;

	//C11 = M1 + M4 - M5 + M7
	sum((int *) &M1[0], (int *) &M4[0], (int *) &tmpA[0], newSize, newSize);
	sub((int *) &M7[0], (int *) &M5[0], (int *) &tmpB[0], newSize, newSize);
	sum((int *) &tmpA[0], (int *) &tmpB[0], (int *) &C[p11], newSize, size);

	//C12 = M3 + M5
	sum((int *) &M3[0], (int *) &M5[0], (int *) &C[p12], newSize, size);
	
	//C21 = M2 + M4
	sum((int *) &M2[0], (int *) &M4[0], (int *) &C[p21], newSize, size);

	//C22 = M1 - M2 + M3 + M6
	sub((int *) &M1[0], (int *) &M2[0], (int *) &tmpA[0], newSize, newSize);
	sum((int *) &M3[0], (int *) &M6[0], (int *) &tmpB[0], newSize, newSize);
	sum((int *) &tmpA[0], (int *) &tmpB[0], (int *) &C[p22], newSize, size);
    }
}

long getTime(struct timeval *t1, struct timeval *t2) {
    long elapsed = (t2->tv_sec - t1->tv_sec) * 1000L;
    elapsed += (t2->tv_usec - t1->tv_usec) / 1000L;
    return elapsed;
}

int main(int argc, char *argv[]) {
    char testing;
    if (argc > 2) {
	N = atoi(argv[1]);
	testing = *argv[2];
    }
    else N = 512;
    
    //Timing
    float time, timeS, speedUp;
    struct timeval t1, t2;

    //Matrix
    int *A, *B, *C, *D;
    
    A = malloc(N*N*sizeof(int));
    B = malloc(N*N*sizeof(int));
    C = malloc(N*N*sizeof(int));

    init(A, N);
    init(B, N);

    gettimeofday(&t1, NULL);
    strassen(A, B, C, N);
    gettimeofday(&t2, NULL);
    timeS = (float) getTime(&t1, &t2) / 1000.0;

    printf("Strassen CPU\nN = %d\n", N);
    if(testing=='Y'){
	D = malloc(N*N*sizeof(int));
	gettimeofday(&t1, NULL);
	multiplicate(A, B, D, N);
	gettimeofday(&t2, NULL);
	time = (float)getTime(&t1, &t2) / 1000.0;
    
	check(D, C, N);
	speedUp = (timeS == 0) ? 1.0 : (time / timeS);

	printf("Correct result\n");
	printf("Time MxM ijk = %f sec\nTime Strassen = %f sec\nSpeedUp = %f\n", time, timeS, speedUp);
	free(D);
    }
    else printf("NO TEST\nTime Strassen = %f sec\n", timeS);
    
    free(A); 
    free(B);
    free(C);
}
