#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <stdint.h>

#define BS 8

int64_t N;

__device__ void sumGPU(int64_t *A, int64_t *B, int64_t *C, int64_t dim, int64_t dimC) {
    int64_t row = blockIdx.y * blockDim.y + threadIdx.y;
    int64_t col = blockIdx.x * blockDim.x + threadIdx.x;
     
    if ((row < dimC) && (col < dimC) && (row < dim) && (col < dim)) {
	int64_t pos = row*dim + col, posC = row*dimC + col;
	C[posC] = A[pos] + B[pos];
    }
}

__device__ void subGPU(int64_t *A, int64_t *B, int64_t *C, int64_t dim, int64_t dimC) {
    int64_t row = blockIdx.y * blockDim.y + threadIdx.y;
    int64_t col = blockIdx.x * blockDim.x + threadIdx.x;
    
    if ((row < dimC) && (col < dimC) && (row < dim) && (col < dim)) {
	int64_t pos = row*dim + col, posC = row*dimC + col;
        C[posC] = A[pos]-B[pos];    
    }
}

__device__ void multGPU(int64_t *A, int64_t *B, int64_t *C, int64_t dimA, int64_t dimB, int64_t dimC) {
    int64_t row = blockIdx.y * blockDim.y + threadIdx.y;
    int64_t col = blockIdx.x * blockDim.x + threadIdx.x;
    
    if ((row < dimC) && (col < dimC)) {
	int64_t k;
	int64_t tmp = 0;
	int64_t pos = row * dimC + col;
	int64_t posA = row*dimA;
	int64_t posB = dimB+col;
	for (k = 0; k < dimC; ++k) {
	    tmp += A[row*dimA + k] * B[k*dimB + col];	
	}
	C[pos] = tmp;
	//C[pos] = posB;
    }
}

__device__ void printGPU(int64_t *A, int64_t dim) {
    int64_t row = blockIdx.y * blockDim.y + threadIdx.y;
    int64_t col = blockIdx.x * blockDim.x + threadIdx.x;
    if ((row + col) == 0) {
	int64_t i, j;	
	for (i = 0; i < dim; ++i) {
	    for (j = 0; j < dim; ++j) {
		printf("%d ", A[i*dim + j]);
		if ((j+1)%16 == 0) printf("\n");
	    }
	    printf("\n");
	}
    }
    __syncthreads();
}

//__device__ int32_t *M1, *M2, *M3, *M4, *M5, *M6, *M7, *tmpA, *tmpB;

__global__ void strassenGPU(int64_t *A, int64_t *B, int64_t *C, int64_t size, int64_t *M1, int64_t *M2, int64_t *M3, int64_t *M4, int64_t *M5, int64_t *M6, int64_t *M7, int64_t *tmpA, int64_t *tmpB, int64_t *tmpC, int64_t *tmpD, int64_t *tmpE, int64_t *tmpF, int64_t *tmpG, int64_t *tmpH, int64_t *tmpI, int64_t *tmpJ) {
    int64_t row = blockIdx.y * blockDim.y + threadIdx.y;
    int64_t col = blockIdx.x * blockDim.x + threadIdx.x;
    //int pos = row * size + col;
    int64_t newSize = (size >> 1);

    /*if ((row + col) == 0) { //Thread 0
	M1 = (int32_t *) malloc(newSize*newSize*sizeof(int32_t));
	M2 = (int32_t *) malloc(newSize*newSize*sizeof(int32_t));
	M3 = (int32_t *) malloc(newSize*newSize*sizeof(int32_t));
	M4 = (int32_t *) malloc(newSize*newSize*sizeof(int32_t));
	M5 = (int32_t *) malloc(newSize*newSize*sizeof(int32_t));
	M6 = (int32_t *) malloc(newSize*newSize*sizeof(int32_t));
	M7 = (int32_t *) malloc(newSize*newSize*sizeof(int32_t));
	tmpA = (int32_t *) malloc(newSize*newSize*sizeof(int32_t));
	tmpB = (int32_t *) malloc(newSize*newSize*sizeof(int32_t));
    }
    __syncthreads();*/
    
    //Pointers to subMatrix
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;

    /*//M3 = A11 * (B12 - B22)
    subGPU((int64_t *) &B[p12], (int64_t *) &B[p22], tmpB, size, newSize);
    __syncthreads();
    //multGPU((int64_t *) &A[p11], tmpB, M3, size, newSize, newSize);   
    multGPU((int64_t *) &A[p11], tmpB, M3, size, newSize, newSize);   
    __syncthreads();*/

    //M1 = (A11 + A22) * (B11 + B22)
    sumGPU((int64_t *) &A[p11], (int64_t *) &A[p22], tmpA, size, newSize);    
    sumGPU((int64_t *) &B[p11], (int64_t *) &B[p22], tmpB, size, newSize);
    __syncthreads();

    //M2 = (A21 + A22) * B11
    sumGPU((int64_t *) &A[p21], (int64_t *) &A[p22], tmpC, size, newSize);
    __syncthreads();

    //M3 = A11 * (B12 - B22)
    subGPU((int64_t *) &B[p12], (int64_t *) &B[p22], tmpD, size, newSize);
    __syncthreads();
    //multGPU((int64_t *) &A[p11], tmpB, M3, size, newSize, newSize);   

    //M4 = A22 * (B21 - B11)
    subGPU((int64_t *) &B[p21], (int64_t *) &B[p11], tmpE, size, newSize);
    __syncthreads();

    //printGPU(M4, newSize);
     
    //M5 = (A11 + A12) * B22
    sumGPU((int64_t *) &A[p11], (int64_t *) &A[p12], tmpF, size, newSize);
    __syncthreads();

    //M6 = (A21 - A11) * (B11 + B12)
    subGPU((int64_t *) &A[p21], (int64_t *) &A[p11], tmpG, size, newSize);
    sumGPU((int64_t *) &B[p11], (int64_t *) &B[p12], tmpH, size, newSize);
    __syncthreads();

    //M7 = (A12 - A22) * (B21 + B22)
    subGPU((int64_t *) &A[p12], (int64_t *) &A[p22], tmpI, size, newSize);
    sumGPU((int64_t *) &B[p21], (int64_t *) &B[p22], tmpJ, size, newSize);
    __syncthreads();

    multGPU(tmpA, tmpB, M1, newSize, newSize, newSize);
    __syncthreads();

    multGPU(tmpC, (int64_t *) &B[p11], M2, newSize, size, newSize);
    __syncthreads();
    
    multGPU((int64_t *) &A[p11], tmpD, M3, size, newSize, newSize);   
    __syncthreads();
    
    multGPU((int64_t *) &A[p22], tmpE, M4, size, newSize, newSize);
    __syncthreads();
    
    multGPU(tmpF, (int64_t *) &B[p22], M5, newSize, size, newSize);
    __syncthreads();
    
    multGPU(tmpG, tmpH, M6, newSize, newSize, newSize);
    __syncthreads();
    
    multGPU(tmpI, tmpJ, M7, newSize, newSize, newSize);
    __syncthreads();
    //C11 = M1 + M4 - M5 + M7
    sumGPU(M1, M4, tmpA, newSize, newSize);
    subGPU(M7, M5, tmpB, newSize, newSize);
    __syncthreads();
    sumGPU(tmpA, tmpB, (int64_t *) &C[p11], newSize, size);
    __syncthreads();

    //C12 = M3 + M5
    sumGPU(M3, M5, (int64_t *) &C[p12], newSize, size);
    __syncthreads();

    //C21 = M2 + M4
    sumGPU(M2, M4, (int64_t *) &C[p21], newSize, size);
    __syncthreads();

    //C22 = M1 - M2 + M3 + M6
    subGPU(M1, M2, tmpA, newSize, newSize);
    sumGPU(M3, M6, tmpB, newSize, newSize);
    __syncthreads();
    sumGPU(tmpA, tmpB, (int64_t *) &C[p22], newSize, size);
    __syncthreads();
    /*if ((row + col) == 0) { //Thread 0
	free(M1);
	free(M2);
	free(M3);
	free(M4);
	free(M5);
	free(M6);
	free(M7);
	free(tmpA);
	free(tmpB);
    }*/
}

__global__ void sumStrassenGPU(int64_t *A, int64_t *B, int64_t size, int64_t *tmpA, int64_t *tmpB, int64_t *tmpC, int64_t *tmpD, int64_t *tmpE, int64_t *tmpF, int64_t *tmpG, int64_t *tmpH, int64_t *tmpI, int64_t *tmpJ) {
    int64_t row = blockIdx.y * blockDim.y + threadIdx.y;
    int64_t col = blockIdx.x * blockDim.x + threadIdx.x;
    int64_t newSize = (size >> 1);

    //Pointers to subMatrix
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;

    //M1 = (A11 + A22) * (B11 + B22)
    sumGPU((int64_t *) &A[p11], (int64_t *) &A[p22], tmpA, size, newSize);    
    sumGPU((int64_t *) &B[p11], (int64_t *) &B[p22], tmpB, size, newSize);

    //M2 = (A21 + A22) * B11
    sumGPU((int64_t *) &A[p21], (int64_t *) &A[p22], tmpC, size, newSize);

    //M3 = A11 * (B12 - B22)
    subGPU((int64_t *) &B[p12], (int64_t *) &B[p22], tmpD, size, newSize);

    //M4 = A22 * (B21 - B11)
    subGPU((int64_t *) &B[p21], (int64_t *) &B[p11], tmpE, size, newSize);

    //M5 = (A11 + A12) * B22
    sumGPU((int64_t *) &A[p11], (int64_t *) &A[p12], tmpF, size, newSize);

    //M6 = (A21 - A11) * (B11 + B12)
    subGPU((int64_t *) &A[p21], (int64_t *) &A[p11], tmpG, size, newSize);
    sumGPU((int64_t *) &B[p11], (int64_t *) &B[p12], tmpH, size, newSize);

    //M7 = (A12 - A22) * (B21 + B22)
    subGPU((int64_t *) &A[p12], (int64_t *) &A[p22], tmpI, size, newSize);
    sumGPU((int64_t *) &B[p21], (int64_t *) &B[p22], tmpJ, size, newSize);
}

__global__ void multStrassenGPU(int64_t *A, int64_t *B, int64_t size, int64_t *M1, int64_t *M2, int64_t *M3, int64_t *M4, int64_t *M5, int64_t *M6, int64_t *M7, int64_t *tmpA, int64_t *tmpB, int64_t *tmpC, int64_t *tmpD, int64_t *tmpE, int64_t *tmpF, int64_t *tmpG, int64_t *tmpH, int64_t *tmpI, int64_t *tmpJ) {
    int64_t newSize = (size >> 1);
    //Pointers to subMatrix
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;

    multGPU(tmpA, tmpB, M1, newSize, newSize, newSize);
    multGPU(tmpC, (int64_t *) &B[p11], M2, newSize, size, newSize);
    multGPU((int64_t *) &A[p11], tmpD, M3, size, newSize, newSize);   
    multGPU((int64_t *) &A[p22], tmpE, M4, size, newSize, newSize);
    multGPU(tmpF, (int64_t *) &B[p22], M5, newSize, size, newSize);
    multGPU(tmpG, tmpH, M6, newSize, newSize, newSize);
    multGPU(tmpI, tmpJ, M7, newSize, newSize, newSize);
}

__global__ void resultStrassenGPU(int64_t *C, int64_t size, int64_t *M1, int64_t *M2, int64_t *M3, int64_t *M4, int64_t *M5, int64_t *M6, int64_t *M7, int64_t *tmpA, int64_t *tmpB) {
    int64_t newSize = (size >> 1);
    //Pointers to subMatrix
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;

    //C11 = M1 + M4 - M5 + M7
    sumGPU(M1, M4, tmpA, newSize, newSize);
    subGPU(M7, M5, tmpB, newSize, newSize);
    sumGPU(tmpA, tmpB, (int64_t *) &C[p11], newSize, size);

    //C12 = M3 + M5
    sumGPU(M3, M5, (int64_t *) &C[p12], newSize, size);

    //C21 = M2 + M4
    sumGPU(M2, M4, (int64_t *) &C[p21], newSize, size);

    //C22 = M1 - M2 + M3 + M6
    subGPU(M1, M2, tmpA, newSize, newSize);
    sumGPU(M3, M6, tmpB, newSize, newSize);
    sumGPU(tmpA, tmpB, (int64_t *) &C[p22], newSize, size);
}

void init(int64_t *matrix, const int64_t dim) {
    int64_t i;
    for (i = 0; i < dim*dim; ++i) {
	matrix[i] = 1;
    }
}

bool check(int64_t *A, int64_t *B, const int64_t dim) {
    int64_t i;
    for (i = 0; i < (dim*dim); ++i) {
	if (A[i] != B[i]) return false;
    }
    return true;
}

void mult(int64_t *A, int64_t *B, int64_t *C, const int64_t dim) {
    int64_t i, j, k, ii, jj;
    int64_t tmp;
    /*for (ii = 0; ii < dim; ii+=BS) {
	for (jj = 0; jj < dim; jj+=BS) {
	    for (i = 0; i < dim; ++i) {
		for (j = ii; j < (ii+BS); ++j) {
		    tmp = 0;
		    for (k = jj; k < (jj+BS); ++k) {
			tmp += A[i*dim + k]*B[k*dim + j];
		    }
		    C[i*dim + j] = tmp;
		}
	    }
	}
    }*/
    for (i = 0; i < dim; ++i) {
	for (j = 0; j < dim; ++j) {
	    tmp = 0;
	    for (k = 0; k < dim; ++k) {
		tmp += A[i*dim + k] * B[k*dim + j];
	    }
	    C[i*dim + j] = tmp;
	}
    }
}

void print(int64_t *matrix, const int64_t dim) {
    printf("\n");
    int64_t i, j, k, tmp, tmp1, tmp2;
    
    tmp1 = matrix[0];
    for (i = 1; i < dim*dim; ++i) {
	if (matrix[i] > tmp1) tmp1 = matrix[i];
    }
    tmp2 = snprintf(NULL, 0, "%ld", tmp1);

    for (i = 0; i < dim; ++i) {
	for (j = 0; j < dim; ++j) {
	    tmp = matrix[i*dim + j];
	    tmp1 = tmp2 - snprintf(NULL, 0, "%ld", tmp) + 1;
	    printf("%ld", tmp);
	    for (k = 0; k < tmp1; ++k) printf(" ");
	    if ((j+1) % 16 == 0) {
		printf("\n");
	    }
	}
	printf("\n\n");
    }
}

void printSubMatrix(int64_t *matrix, const int64_t dim) {
    printf("\n");
    int64_t i, j, k, tmp, tmp1, tmp2;
    
    tmp1 = matrix[0];
    for (i = 1; i < dim*dim; ++i) {
	if (matrix[i] > tmp1) tmp1 = matrix[i];
    }
    tmp2 = snprintf(NULL, 0, "%ld", tmp1);
   
    printf("submatrix 11\n"); 
    for (i = 0; i < (dim >> 1); ++i) {
	for (j = 0; j < (dim >> 1); ++j) {
	    tmp = matrix[i*dim + j];
	    tmp1 = tmp2 - snprintf(NULL, 0, "%ld", tmp) + 1;
	    printf("%ld", tmp);
	    for (k = 0; k < tmp1; ++k) printf(" ");
	    if ((j+1) % 16 == 0) {
		printf("\n");
	    }
	}
	printf("\n\n");
    }
    
    printf("submatrix 12\n");
    for (i = 0; i < (dim >> 1); ++i) {
	for (j = 0; j < (dim >> 1); ++j) {
	    tmp = matrix[i*dim + j + (dim>>1)];
	    tmp1 = tmp2 - snprintf(NULL, 0, "%ld", tmp) + 1;
	    printf("%ld", tmp);
	    for (k = 0; k < tmp1; ++k) printf(" ");
	    if ((j+1) % 16 == 0) {
		printf("\n");
	    }
	}
	printf("\n\n");
    }

    printf("submatrix 21\n");
    for (i = 0; i < (dim >> 1); ++i) {
	for (j = 0; j < (dim >> 1); ++j) {
	    tmp = matrix[(i+(dim>>1))*dim + j];
	    tmp1 = tmp2 - snprintf(NULL, 0, "%ld", tmp) + 1;
	    printf("%ld", tmp);
	    for (k = 0; k < tmp1; ++k) printf(" ");
	    if ((j+1) % 16 == 0) {
		printf("\n");
	    }
	}
	printf("\n\n");
    }

    printf("submatrix 22\n");
    for (i = 0; i < (dim >> 1); ++i) {
	for (j = 0; j < (dim >> 1); ++j) {
	    tmp = matrix[(i+(dim>>1))*dim + j + (dim>>1)];
	    tmp1 = tmp2 - snprintf(NULL, 0, "%ld", tmp) + 1;
	    printf("%ld", tmp);
	    for (k = 0; k < tmp1; ++k) printf(" ");
	    if ((j+1) % 16 == 0) {
		printf("\n");
	    }
	}
	printf("\n\n");
    }
}

long getTime(struct timeval *t1, struct timeval *t2) {
    long elapsed = (t2->tv_sec - t1->tv_sec) * 1000L;
    elapsed += (t2->tv_usec - t1->tv_usec) / 1000L;
    return elapsed;
}

void checkError(cudaError_t cudaError, char *s, int64_t line) {
    if (cudaError != cudaSuccess) {
	printf("%s failed with error code %d at line %d\n", s, cudaError, line);
	printf("%s\n", cudaGetErrorString(cudaError));
	abort();
    }
}

int main(int argc, char *argv[]) {
    char testing;
    if (argc > 2) {
	N = atoi(argv[1]);
	testing = *argv[2];
    }
    else N = 512;
    
    float TiempoTotal, TiempoKernel;
    cudaEvent_t E0, E1, E2, E3;
    int64_t nBlocks, nThreads;
    /*cudaError_t cError;
    //Timing
    float time, timeS, speedUp;
    struct timeval t1, t2;*/

    //Matrix
    int64_t *h_A, *h_B, *h_C;
    int64_t *d_A, *d_B, *d_C;
    int64_t *M1, *M2, *M3, *M4, *M5, *M6, *M7, *tmpA, *tmpB;
    int64_t *tmpC, *tmpD, *tmpE, *tmpF, *tmpG, *tmpH, *tmpI, *tmpJ;
    int64_t *h_M1, *h_M2, *h_M3, *h_M4, *h_M5, *h_M6, *h_M7;

    nThreads = 32;
    nBlocks = (N+nThreads-1)/nThreads;
    int64_t numBytes = N*N*sizeof(int64_t);
    int64_t numBytes_tmp = (N/2)*(N/2)*sizeof(int64_t);
    
    dim3 dimGrid(nBlocks, nBlocks, 1);
    dim3 dimBlock(nThreads, nThreads, 1);
    
    cudaEventCreate(&E0);
    cudaEventCreate(&E1);
    cudaEventCreate(&E2);
    cudaEventCreate(&E3);
    
    //host
    h_A = (int64_t *) malloc(N*N*sizeof(int64_t));
    h_B = (int64_t *) malloc(N*N*sizeof(int64_t));
    h_C = (int64_t *) malloc(N*N*sizeof(int64_t));


    //device
    cudaMalloc((int64_t **)&d_A, numBytes);
    checkError(cudaGetLastError(), "cudaMalloc 1", __LINE__);
    cudaMalloc((int64_t **)&d_B, numBytes);
    checkError(cudaGetLastError(), "cudaMalloc 2", __LINE__);
    cudaMalloc((int64_t **)&d_C, numBytes);
    checkError(cudaGetLastError(), "cudaMalloc 3", __LINE__);
    cudaMalloc((int64_t **)&M1, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 4", __LINE__);
    cudaMalloc((int64_t **)&M2, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 5", __LINE__);
    cudaMalloc((int64_t **)&M3, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 6", __LINE__);
    cudaMalloc((int64_t **)&M4, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 7", __LINE__);
    cudaMalloc((int64_t **)&M5, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 8", __LINE__);
    cudaMalloc((int64_t **)&M6, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 9", __LINE__);
    cudaMalloc((int64_t **)&M7, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 10", __LINE__);
    cudaMalloc((int64_t **)&tmpA, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 11", __LINE__);
    cudaMalloc((int64_t **)&tmpB, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 12", __LINE__);
    cudaMalloc((int64_t **)&tmpC, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 13", __LINE__);
    cudaMalloc((int64_t **)&tmpD, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 13", __LINE__);
    cudaMalloc((int64_t **)&tmpE, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 13", __LINE__);
    cudaMalloc((int64_t **)&tmpF, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 13", __LINE__);
    cudaMalloc((int64_t **)&tmpG, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 13", __LINE__);
    cudaMalloc((int64_t **)&tmpH, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 13", __LINE__);
    cudaMalloc((int64_t **)&tmpI, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 13", __LINE__);
    cudaMalloc((int64_t **)&tmpJ, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 13", __LINE__);
     
    //init
    init(h_A, N);
    init(h_B, N);

    cudaEventRecord(E0, 0);
    cudaEventSynchronize(E0);

    //Copy to device
    cudaMemcpy(d_A, h_A, numBytes, cudaMemcpyHostToDevice); 
    checkError(cudaGetLastError(), "cudaMemcpy 1", __LINE__);
    cudaMemcpy(d_B, h_B, numBytes, cudaMemcpyHostToDevice);
    checkError(cudaGetLastError(), "cudaMemcpy 2", __LINE__);

    cudaEventRecord(E1, 0);
    cudaEventSynchronize(E1);

    sumStrassenGPU<<<dimGrid, dimBlock>>>(d_A, d_B, N, tmpA, tmpB, tmpC, tmpD, tmpE, tmpF, tmpG, tmpH, tmpI, tmpJ);
    multStrassenGPU<<<dimGrid, dimBlock>>>(d_A, d_B, N, M1, M2, M3, M4, M5, M6, M7, tmpA, tmpB, tmpC, tmpD, tmpE, tmpF, tmpG, tmpH, tmpI, tmpJ);
    resultStrassenGPU<<<dimGrid, dimBlock>>>(d_C, N, M1, M2, M3, M4, M5, M6, M7, tmpA, tmpB);
    checkError(cudaGetLastError(), "kernel execution", __LINE__);
        
    cudaEventRecord(E2, 0);
    cudaEventSynchronize(E2);

    cudaMemcpy(h_C, d_C, numBytes, cudaMemcpyDeviceToHost);
    checkError(cudaGetLastError(), "cudaMemcpy result", __LINE__);

    //Free device memory
    cudaFree(d_A);
    cudaFree(d_B);
    cudaFree(d_C);
    cudaFree(M1);
    cudaFree(M2);
    cudaFree(M3);
    cudaFree(M4);
    cudaFree(M5);
    cudaFree(M6);
    cudaFree(M7);
    cudaFree(tmpA);
    cudaFree(tmpB);
    checkError(cudaGetLastError(), "cudaFree", __LINE__);

    cudaEventRecord(E3, 0);
    cudaEventSynchronize(E3);

    //Check result
    int64_t *h_D = (int64_t *) malloc(N*N*sizeof(int64_t));
    if(testing == 'Y'){	
	mult(h_A, h_B, h_D, N);
	bool result = check(h_D, h_C, N);
	if (result) {
	    printf("Correct result\n");
	}
	else {
	    printf("Wrong result\n");
	}
    }
    cudaEventElapsedTime(&TiempoTotal,  E0, E3);
    cudaEventElapsedTime(&TiempoKernel, E1, E2);
    printf("\nSTRASSEN 1 GPU\n");
    printf("Dimensiones: %dx%d\n", N, N);
    printf("nThreads: %dx%d (%d)\n", nThreads, nThreads, nThreads * nThreads);
    printf("nBlocks: %dx%d (%d)\n", nBlocks, nBlocks, nBlocks*nBlocks);
    /*if (PINNED) printf("Usando Pinned Memory\n");
    else printf("NO usa Pinned Memory\n");*/
    printf("Tiempo Global: %4.6f milseg\n", TiempoTotal);
    printf("Tiempo Kernel: %4.6f milseg\n", TiempoKernel);

    //Free host memory
    free(h_A); 
    free(h_B);
    free(h_C);
    free(h_D);
}
