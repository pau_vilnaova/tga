CUDA_HOME   = /Soft/cuda/7.5.18

NVCC        = $(CUDA_HOME)/bin/nvcc
NVCC_FLAGS  = -O3 -I$(CUDA_HOME)/include -arch=compute_35 --ptxas-options=-v -I$(CUDA_HOME)/sdk/CUDALibraries/common/inc
LD_FLAGS    = -lcudart -Xlinker -rpath,$(CUDA_HOME)/lib64 -I$(CUDA_HOME)/sdk/CUDALibraries/common/lib
PROG_FLAGS  = -DSIZE=32

EXE    = strassenCPU.exe strassenGPU.exe strassenGPU00.exe strassen2GPU.exe strassen2GPU01.exe strassen4GPU.exe strassen4GPU01.exe

OBJ    = strassenCPU.o strassenGPU.o strassenGPU00.o strassen2GPU.o strassen2GPU01.exe strassen4GPU.o strassen4GPU01.o

default: $(EXE)

#CPU
strassenCPU.o: strassenCPU.c
	$(NVCC) -c -w -o $@ strassenCPU.c $(NVCC_FLAGS) $(PROG_FLAGS)

strassenCPU.exe: strassenCPU.o
	$(NVCC) strassenCPU.o -w -o strassenCPU.exe $(LD_FLAGS)

#GPU
strassenGPU.o: strassenGPU.cu
	$(NVCC) -c -w -o $@ strassenGPU.cu $(NVCC_FLAGS) $(PROG_FLAGS)


strassenGPU.exe: strassenGPU.o
	$(NVCC) strassenGPU.o -w -o strassenGPU.exe $(LD_FLAGS)

#2GPU
strassen2GPU.o: strassen2GPU.cu
	$(NVCC) -c -w -o $@ strassen2GPU.cu $(NVCC_FLAGS) $(PROG_FLAGS)

strassen2GPU.exe: strassen2GPU.o
	$(NVCC) strassen2GPU.o -w -o strassen2GPU.exe $(LD_FLAGS)

strassen2GPU01.o: strassen2GPU01.cu
	$(NVCC) -c -w -o $@ strassen2GPU01.cu $(NVCC_FLAGS) $(PROG_FLAGS)

strassen2GPU01.exe: strassen2GPU01.o
	$(NVCC) strassen2GPU01.o -w -o strassen2GPU01.exe $(LD_FLAGS)

#4GPU
strassen4GPU.o: strassen4GPU.cu
	$(NVCC) -c -w -o $@ strassen4GPU.cu $(NVCC_FLAGS) $(PROG_FLAGS)

strassen4GPU.exe: strassen4GPU.o
	$(NVCC) strassen4GPU.o -w -o strassen4GPU.exe $(LD_FLAGS)

#4GPU1
strassen4GPU01.o: strassen4GPU01.cu
	$(NVCC) -c -w -o $@ strassen4GPU01.cu $(NVCC_FLAGS) $(PROG_FLAGS)

strassen4GPU01.exe: strassen4GPU01.o
	$(NVCC) strassen4GPU01.o -w -o strassen4GPU01.exe $(LD_FLAGS)

#GPU00
strassenGPU00.o: strassenGPU00.cu
	$(NVCC) -c -w -o $@ strassenGPU00.cu $(NVCC_FLAGS) $(PROG_FLAGS)

strassenGPU00.exe: strassenGPU00.o
	$(NVCC) strassenGPU00.o -w -o strassenGPU00.exe $(LD_FLAGS)



all:	$(EXE) 

clean:
	rm -rf *.o *.exe *.e* *.o*

