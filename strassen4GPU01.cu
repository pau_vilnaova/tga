#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <stdint.h>

#define SIZE 32
#define BS 8

int64_t N;
//Size M = Size A * 2 = Size B * 2
__global__ void kernelM1(int64_t *A, int64_t *B, int64_t *M, int64_t size) {
    //Thread identifiers
    int64_t ty = threadIdx.y;
    int64_t tx = threadIdx.x;
    int64_t row = blockIdx.y * SIZE + ty;
    int64_t col = blockIdx.x * SIZE + tx;


    //Pointers to subMatrix
    int newSize = size >> 1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;

    __shared__ int64_t tmpA[SIZE][SIZE];
    __shared__ int64_t tmpB[SIZE][SIZE];

    int64_t tmp = 0;
    int64_t lim = (SIZE > newSize) ? newSize : SIZE;
    
    for (int m = 0; m < newSize; m += SIZE) {
	//Add before mult
	tmpA[ty][tx] = A[row*size + m + tx] + A[p22 + (row * size) + m + tx]; //A11 + A22
	tmpB[ty][tx] = B[(m+ty)*size + col] + B[p22 + (m+ty)*size + col]; //B11 + B22
	__syncthreads();

	//M1 = (A11 + A22) * (B11 + B22)
	for (int k = 0; k < lim; ++k) {
	    tmp += tmpA[ty][k] * tmpB[k][tx];
	}
	__syncthreads();
    }
    M[row * newSize + col] = tmp;
}

//Size M = Size A * 2 = Size B * 2
__global__ void kernelM2(int64_t *A, int64_t *B, int64_t *M, int64_t size) {
    //Thread identifiers
    int64_t ty = threadIdx.y;
    int64_t tx = threadIdx.x;
    int64_t row = blockIdx.y * SIZE + ty;
    int64_t col = blockIdx.x * SIZE + tx;

    //Pointers to subMatrix
    int newSize = size >> 1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;

    __shared__ int64_t tmpA[SIZE][SIZE];
    __shared__ int64_t tmpB[SIZE][SIZE];

    int64_t tmp = 0;
    int64_t lim = (SIZE > newSize) ? newSize : SIZE;

    for (int m = 0; m < newSize; m += SIZE) {
	//Add before mult
	tmpA[ty][tx] = A[p21 + row*size + m + tx] + A[p22 + (row * size) + m + tx]; //A21 + A22
	tmpB[ty][tx] = B[(m+ty)*size + col]; //B11
	__syncthreads();

	//M2 = (A21 + A22) * B11
	for (int k = 0; k < lim; ++k) {
	    tmp += tmpA[ty][k] * tmpB[k][tx];
	}
	__syncthreads();
    }
    M[row * newSize + col] = tmp;
}

//Size M = Size A * 2 = Size B * 2
__global__ void kernelM3(int64_t *A, int64_t *B, int64_t *M, int64_t size) {
    //Thread identifiers
    int64_t ty = threadIdx.y;
    int64_t tx = threadIdx.x;
    int64_t row = blockIdx.y * SIZE + ty;
    int64_t col = blockIdx.x * SIZE + tx;

    //Pointers to subMatrix
    int newSize = size >> 1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;

    __shared__ int64_t tmpA[SIZE][SIZE];
    __shared__ int64_t tmpB[SIZE][SIZE];

    int64_t tmp = 0;
    int64_t lim = (SIZE > newSize) ? newSize : SIZE;

    for (int m = 0; m < newSize; m += SIZE) {
	//Add before mult
	tmpA[ty][tx] = A[row*size + m + tx]; //A11
	tmpB[ty][tx] = B[p12 + (m+ty)*size + col] - B[p22 + (m+ty)*size + col]; //B12 - B22
	__syncthreads();

	//M3 = A11 * (B12 - B22)
	for (int k = 0; k < lim; ++k) {
	    tmp += tmpA[ty][k] * tmpB[k][tx];
	}
	__syncthreads();
    }
    M[row * newSize + col] = tmp;
}

//Size M = Size A * 2 = Size B * 2
__global__ void kernelM4(int64_t *A, int64_t *B, int64_t *M, int64_t size) {
    //Thread identifiers
    int64_t ty = threadIdx.y;
    int64_t tx = threadIdx.x;
    int64_t row = blockIdx.y * SIZE + ty;
    int64_t col = blockIdx.x * SIZE + tx;

    //Pointers to subMatrix
    int newSize = size >> 1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;

    __shared__ int64_t tmpA[SIZE][SIZE];
    __shared__ int64_t tmpB[SIZE][SIZE];

    int64_t tmp = 0;
    int64_t lim = (SIZE > newSize) ? newSize : SIZE;

    for (int m = 0; m < newSize; m += SIZE) {
	//Add before mult
	tmpA[ty][tx] = A[p22 + row*size + m + tx]; //A22
	tmpB[ty][tx] = B[p21 + (m+ty)*size + col] - B[(m+ty)*size + col]; //B21 - B11
	__syncthreads();

	//M4 = A11 * (B12 - B22)
	for (int k = 0; k < lim; ++k) {
	    tmp += tmpA[ty][k] * tmpB[k][tx];
	}
	__syncthreads();
    }
    M[row * newSize + col] = tmp;
}

//Size M = Size A * 2 = Size B * 2
__global__ void kernelM5(int64_t *A, int64_t *B, int64_t *M, int64_t size) {
    //Thread identifiers
    int64_t ty = threadIdx.y;
    int64_t tx = threadIdx.x;
    int64_t row = blockIdx.y * SIZE + ty;
    int64_t col = blockIdx.x * SIZE + tx;

    //Pointers to subMatrix
    int newSize = size >> 1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;

    __shared__ int64_t tmpA[SIZE][SIZE];
    __shared__ int64_t tmpB[SIZE][SIZE];

    int64_t tmp = 0;
    int64_t lim = (SIZE > newSize) ? newSize : SIZE;

    for (int m = 0; m < newSize; m += SIZE) {
	//Add before mult
	tmpA[ty][tx] = A[row*size + m + tx] + A[p12 + row*size + m + tx]; //A11 + A12
	tmpB[ty][tx] = B[p22 + (m+ty)*size + col]; //B22
	__syncthreads();

	//M5 = (A11 + A12) * (B22)
	for (int k = 0; k < lim; ++k) {
	    tmp += tmpA[ty][k] * tmpB[k][tx];
	}
	__syncthreads();
    }
    M[row * newSize + col] = tmp;
}

//Size M = Size A * 2 = Size B * 2
__global__ void kernelM6(int64_t *A, int64_t *B, int64_t *M, int64_t size) {
    //Thread identifiers
    int64_t ty = threadIdx.y;
    int64_t tx = threadIdx.x;
    int64_t row = blockIdx.y * SIZE + ty;
    int64_t col = blockIdx.x * SIZE + tx;

    //Pointers to subMatrix
    int newSize = size >> 1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;

    __shared__ int64_t tmpA[SIZE][SIZE];
    __shared__ int64_t tmpB[SIZE][SIZE];

    int64_t tmp = 0;
    int64_t lim = (SIZE > newSize) ? newSize : SIZE;

    for (int m = 0; m < newSize; m += SIZE) {
	//Add before mult
	tmpA[ty][tx] = A[p21 + row*size + m + tx] - A[row*size + m + tx]; //A21 + A11
	tmpB[ty][tx] = B[(m+ty)*size + col] + B[p12 + (m+ty)*size + col]; //B11 + B12
	__syncthreads();

	//M6 = (A21 - A11) * (B11 + B12)
	for (int k = 0; k < lim; ++k) {
	    tmp += tmpA[ty][k] * tmpB[k][tx];
	}
	__syncthreads();
    }
    M[row * newSize + col] = tmp;
}

//Size M = Size A * 2 = Size B * 2
__global__ void kernelM7(int64_t *A, int64_t *B, int64_t *M, int64_t size) {
    //Thread identifiers
    int64_t ty = threadIdx.y;
    int64_t tx = threadIdx.x;
    int64_t row = blockIdx.y * SIZE + ty;
    int64_t col = blockIdx.x * SIZE + tx;

    //Pointers to subMatrix
    int newSize = size >> 1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;

    __shared__ int64_t tmpA[SIZE][SIZE];
    __shared__ int64_t tmpB[SIZE][SIZE];

    int64_t tmp = 0;
    int64_t lim = (SIZE > newSize) ? newSize : SIZE;

    for (int m = 0; m < newSize; m += SIZE) {
	//Add before mult
	tmpA[ty][tx] = A[p12 + row*size + m + tx] - A[p22 + row*size + m + tx]; //A12 - A22
	tmpB[ty][tx] = B[p21 + (m+ty)*size + col] + B[p22 + (m+ty)*size + col]; //B21 + B22
	__syncthreads();

	//M6 = (A12 - A22) * (B21 + B22)
	for (int k = 0; k < lim; ++k) {
	    tmp += tmpA[ty][k] * tmpB[k][tx];
	}
	__syncthreads();
    }
    M[row * newSize + col] = tmp;
}

__global__ void kernelC11(int64_t *C, int64_t size, int64_t *M1, int64_t *M4, int64_t *M5, int64_t *M7) {
    int64_t row = blockIdx.y * blockDim.y + threadIdx.y;
    int64_t col = blockIdx.x * blockDim.x + threadIdx.x;

    //Pointers to subMatrix
    int newSize = size >> 1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;
    int64_t pos = row*newSize + col;

    if (row < newSize && col < newSize) {
	C[pos] = M1[pos] + M4[pos] - M5[pos] + M7[pos];
    }
}

__global__ void kernelC12(int64_t *C, int64_t size, int64_t *M3, int64_t *M5) {
    int64_t row = blockIdx.y * blockDim.y + threadIdx.y;
    int64_t col = blockIdx.x * blockDim.x + threadIdx.x;

    //Pointers to subMatrix
    int newSize = size >> 1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;
    int64_t pos = row*newSize + col;

    if (row < newSize && col < newSize) {
	C[pos] = M3[pos] + M5[pos];
    }
}

__global__ void kernelC21(int64_t *C, int64_t size, int64_t *M2, int64_t *M4) {
    int64_t row = blockIdx.y * blockDim.y + threadIdx.y;
    int64_t col = blockIdx.x * blockDim.x + threadIdx.x;

    //Pointers to subMatrix
    int newSize = size >> 1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;
    int64_t pos = row*newSize + col;

    if (row < newSize && col < newSize) {
	C[pos] = M2[pos] + M4[pos];
    }
}

__global__ void kernelC22(int64_t *C, int64_t size, int64_t *M1, int64_t *M2, int64_t *M3, int64_t *M6) {
    int64_t row = blockIdx.y * blockDim.y + threadIdx.y;
    int64_t col = blockIdx.x * blockDim.x + threadIdx.x;

    //Pointers to subMatrix
    int newSize = size >> 1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*size), p22 = (newSize*size) + newSize;
    int64_t pos = row*newSize + col;

    if (row < newSize && col < newSize) {
	C[pos] = M1[pos] - M2[pos] + M3[pos] + M6[pos];
    }
}

bool check(int64_t *A, int64_t *B, const int64_t dim) {
    int64_t i;
    for (i = 0; i < (dim*dim); ++i) {
	if (A[i] != B[i]) return false;
    }
    return true;
}

void mult(int64_t *A, int64_t *B, int64_t *C, const int64_t dim) {
    int64_t i, j, k, ii, jj;
    int64_t tmp;
    for (ii = 0; ii < dim; ii+=BS) {
	for (jj = 0; jj < dim; jj+=BS) {
	    for (i = 0; i < dim; ++i) {
		for (j = ii; j < (ii+BS); ++j) {
		    tmp = 0;
		    for (k = jj; k < (jj+BS); ++k) {
			tmp += A[i*dim + k]*B[k*dim + j];
		    }
		    C[i*dim + j] += tmp;
		}
	    }
	}
    }
    /*for (i = 0; i < dim; ++i) {
	for (j = 0; j < dim; ++j) {
	    tmp = 0;
	    for (k = 0; k < dim; ++k) {
		tmp += A[i*dim + k] * B[k*dim + j];
	    }
	    C[i*dim + j] = tmp;
	}
    }*/
}

void print(int64_t *matrix, const int64_t dim) {
    printf("\n");
    int64_t i, j, k, tmp, tmp1, tmp2;
    
    tmp1 = matrix[0];
    for (i = 1; i < dim*dim; ++i) {
	if (matrix[i] > tmp1) tmp1 = matrix[i];
    }
    tmp2 = snprintf(NULL, 0, "%ld", tmp1);

    for (i = 0; i < dim; ++i) {
	for (j = 0; j < dim; ++j) {
	    tmp = matrix[i*dim + j];
	    tmp1 = tmp2 - snprintf(NULL, 0, "%ld", tmp) + 1;
	    printf("%ld", tmp);
	    for (k = 0; k < tmp1; ++k) printf(" ");
	    if ((j+1) % 16 == 0) {
		printf("\n");
	    }
	}
	printf("\n\n");
    }
}

void printSubMatrix(int64_t *matrix, const int64_t dim) {
    printf("\n");
    int64_t i, j, k, tmp, tmp1, tmp2;
    
    tmp1 = matrix[0];
    for (i = 1; i < dim*dim; ++i) {
	if (matrix[i] > tmp1) tmp1 = matrix[i];
    }
    tmp2 = snprintf(NULL, 0, "%ld", tmp1);
   
    printf("submatrix 11\n"); 
    for (i = 0; i < (dim >> 1); ++i) {
	for (j = 0; j < (dim >> 1); ++j) {
	    tmp = matrix[i*dim + j];
	    tmp1 = tmp2 - snprintf(NULL, 0, "%ld", tmp) + 1;
	    printf("%ld", tmp);
	    for (k = 0; k < tmp1; ++k) printf(" ");
	    if ((j+1) % 16 == 0) {
		printf("\n");
	    }
	}
	printf("\n\n");
    }
    
    printf("submatrix 12\n");
    for (i = 0; i < (dim >> 1); ++i) {
	for (j = 0; j < (dim >> 1); ++j) {
	    tmp = matrix[i*dim + j + (dim>>1)];
	    tmp1 = tmp2 - snprintf(NULL, 0, "%ld", tmp) + 1;
	    printf("%ld", tmp);
	    for (k = 0; k < tmp1; ++k) printf(" ");
	    if ((j+1) % 16 == 0) {
		printf("\n");
	    }
	}
	printf("\n\n");
    }

    printf("submatrix 21\n");
    for (i = 0; i < (dim >> 1); ++i) {
	for (j = 0; j < (dim >> 1); ++j) {
	    tmp = matrix[(i+(dim>>1))*dim + j];
	    tmp1 = tmp2 - snprintf(NULL, 0, "%ld", tmp) + 1;
	    printf("%ld", tmp);
	    for (k = 0; k < tmp1; ++k) printf(" ");
	    if ((j+1) % 16 == 0) {
		printf("\n");
	    }
	}
	printf("\n\n");
    }

    printf("submatrix 22\n");
    for (i = 0; i < (dim >> 1); ++i) {
	for (j = 0; j < (dim >> 1); ++j) {
	    tmp = matrix[(i+(dim>>1))*dim + j + (dim>>1)];
	    tmp1 = tmp2 - snprintf(NULL, 0, "%ld", tmp) + 1;
	    printf("%ld", tmp);
	    for (k = 0; k < tmp1; ++k) printf(" ");
	    if ((j+1) % 16 == 0) {
		printf("\n");
	    }
	}
	printf("\n\n");
    }
}

long getTime(struct timeval *t1, struct timeval *t2) {
    long elapsed = (t2->tv_sec - t1->tv_sec) * 1000L;
    elapsed += (t2->tv_usec - t1->tv_usec) / 1000L;
    return elapsed;
}

void checkError(cudaError_t cudaError, char *s, int64_t line) {
    if (cudaError != cudaSuccess) {
	printf("%s failed with error code %d at line %d\n", s, cudaError, line);
	printf("%s\n", cudaGetErrorString(cudaError));
	abort();
    }
}

void init(int64_t *M, const int64_t dim) {
    int i;
    for (i = 0; i < (dim*dim); ++i) {
	M[i] = 1;
    }
}

void merge(int64_t *C11, int64_t *C12, int64_t *C21, int64_t *C22, int64_t *C, int64_t N) {
    int i, j;
    int64_t newSize = N>>1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*N), p22 = (newSize*N) + newSize;

    for (i = 0; i < newSize; ++i) {
	for (j = 0; j < newSize; ++j) {
	    C[i*N + j] = C11[i*newSize + j];
	    C[p12 + i*N + j] = C12[i*newSize + j];
	    C[p21 + i*N + j] = C21[i*newSize + j];
	    C[p22 + i*N + j] = C22[i*newSize + j];
	}
    }
}

int main(int argc, char *argv[]) {
    char testing;
    if (argc > 2) {
	N = atoi(argv[1]);
	testing = *argv[2];
    }
    else N = 512;
    
    float TiempoTotal, TiempoKernel;
    cudaEvent_t E0, E1, E2, E3, X1, X2, X3, S1, S2;
    int64_t nBlocks, nThreads;

    //Host
    int64_t *h_A, *h_B, *h_C11, *h_C12, *h_C21, *h_C22, *h_C;
    //Devices
    int64_t *d_A0, *d_A1, *d_A2, *d_A3;
    int64_t *d_B0, *d_B1, *d_B2, *d_B3;
    int64_t *d_C11, *d_C12, *d_C21, *d_C22;
    //Device 0
    int64_t *M1_0, *M4_0, *M5_0, *M7_0;
    //Device 1
    int64_t *M3_1, *M5_1;
    //Device 2
    int64_t *M2_2, *M4_2;
    //Device 3
    int64_t *M1_3, *M2_3, *M3_3, *M6_3;

    nThreads = 32;
    nBlocks = ((N>>1)+nThreads-1)/nThreads;
    int64_t numBytes = N*N*sizeof(int64_t);
    int64_t numBytes_tmp = (N/2)*(N/2)*sizeof(int64_t);
    int64_t newSize = N>>1;
    int64_t p11 = 0, p12 = newSize, p21 = (newSize*N), p22 = (newSize*N) + newSize;

    dim3 dimGrid(nBlocks, nBlocks, 1);
    dim3 dimBlock(nThreads, nThreads, 1);
  
    int count;
    cudaGetDeviceCount(&count);
    if (count < 4) {
	printf("No hay suficientes GPUs\n");
	exit(0);
    }
     
    //host
    /*h_A = (int64_t *) malloc(N*N*sizeof(int64_t));
    h_B = (int64_t *) malloc(N*N*sizeof(int64_t));
    h_C = (int64_t *) malloc(N*N*sizeof(int64_t));
    h_C11 = (int64_t *) malloc(numBytes_tmp);
    h_C12 = (int64_t *) malloc(numBytes_tmp);
    h_C21 = (int64_t *) malloc(numBytes_tmp);
    h_C22 = (int64_t *) malloc(numBytes_tmp);
    */
    cudaMallocHost((int64_t **)&h_A, (N*N*sizeof(int64_t)));
    cudaMallocHost((int64_t **)&h_B, (N*N*sizeof(int64_t)));
    cudaMallocHost((int64_t **)&h_C, (N*N*sizeof(int64_t)));
    cudaMallocHost((int64_t **)&h_C11, numBytes_tmp);
    cudaMallocHost((int64_t **)&h_C12, numBytes_tmp);
    cudaMallocHost((int64_t **)&h_C21, numBytes_tmp);
    cudaMallocHost((int64_t **)&h_C22, numBytes_tmp);

    //device 0
    cudaSetDevice(0);
    cudaMalloc((int64_t **)&d_A0, numBytes);
    cudaMalloc((int64_t **)&d_B0, numBytes);
    cudaMalloc((int64_t **)&d_C11, numBytes_tmp);
    cudaMalloc((int64_t **)&M1_0, numBytes_tmp);
    cudaMalloc((int64_t **)&M4_0, numBytes_tmp);
    cudaMalloc((int64_t **)&M5_0, numBytes_tmp);
    cudaMalloc((int64_t **)&M7_0, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc", __LINE__);
    
    //device 1
    cudaSetDevice(1);
    cudaMalloc((int64_t **)&d_A1, numBytes);
    cudaMalloc((int64_t **)&d_B1, numBytes);
    cudaMalloc((int64_t **)&d_C12, numBytes_tmp);
    cudaMalloc((int64_t **)&M3_1, numBytes_tmp);
    cudaMalloc((int64_t **)&M5_1, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 3", __LINE__);
    cudaEventCreate(&X1);

    //device 2
    cudaSetDevice(2);
    cudaMalloc((int64_t **)&d_A2, numBytes);
    cudaMalloc((int64_t **)&d_B2, numBytes);
    cudaMalloc((int64_t **)&d_C21, numBytes_tmp);
    cudaMalloc((int64_t **)&M2_2, numBytes_tmp);
    cudaMalloc((int64_t **)&M4_2, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc 3", __LINE__);
    cudaEventCreate(&X2);

    //device 3
    cudaSetDevice(3);
    cudaMalloc((int64_t **)&d_A3, numBytes);
    cudaMalloc((int64_t **)&d_B3, numBytes);
    cudaMalloc((int64_t **)&d_C22, numBytes_tmp);
    cudaMalloc((int64_t **)&M1_3, numBytes_tmp);
    cudaMalloc((int64_t **)&M2_3, numBytes_tmp);
    cudaMalloc((int64_t **)&M3_3, numBytes_tmp);
    cudaMalloc((int64_t **)&M6_3, numBytes_tmp);
    checkError(cudaGetLastError(), "cudaMalloc", __LINE__);
    cudaEventCreate(&X3);

    //init
    init(h_A, N);
    init(h_B, N);
    
    cudaSetDevice(0); 
    cudaEventCreate(&E0);
    cudaEventCreate(&E1);
    cudaEventCreate(&E2);
    cudaEventCreate(&E3);

    cudaEventRecord(E0, 0);
    //cudaEventSynchronize(E0);

    //Copy to device
    cudaMemcpyAsync(d_A0, h_A, numBytes, cudaMemcpyHostToDevice);
    checkError(cudaGetLastError(), "cudaMemcpy 1", __LINE__);
    cudaMemcpyAsync(d_B0, h_B, numBytes, cudaMemcpyHostToDevice);
    checkError(cudaGetLastError(), "cudaMemcpy 2", __LINE__);

    cudaSetDevice(1);
    cudaMemcpyAsync(d_A1, h_A, numBytes, cudaMemcpyHostToDevice); 
    checkError(cudaGetLastError(), "cudaMemcpy 1", __LINE__);
    cudaMemcpyAsync(d_B1, h_B, numBytes, cudaMemcpyHostToDevice);
    checkError(cudaGetLastError(), "cudaMemcpy 2", __LINE__);

    cudaSetDevice(2);
    cudaMemcpyAsync(d_A2, h_A, numBytes, cudaMemcpyHostToDevice); 
    checkError(cudaGetLastError(), "cudaMemcpy 1", __LINE__);
    cudaMemcpyAsync(d_B2, h_B, numBytes, cudaMemcpyHostToDevice);
    checkError(cudaGetLastError(), "cudaMemcpy 2", __LINE__);

    cudaSetDevice(3);
    cudaMemcpyAsync(d_A3, h_A, numBytes, cudaMemcpyHostToDevice); 
    checkError(cudaGetLastError(), "cudaMemcpy 1", __LINE__);
    cudaMemcpyAsync(d_B3, h_B, numBytes, cudaMemcpyHostToDevice);
    checkError(cudaGetLastError(), "cudaMemcpy 2", __LINE__);
    
    cudaSetDevice(0);
    cudaEventRecord(E1, 0);
    cudaEventSynchronize(E1);
    
    cudaSetDevice(0);
    kernelM1<<<dimGrid, dimBlock>>>(d_A0, d_B0, M1_0, N);
    kernelM7<<<dimGrid, dimBlock>>>(d_A0, d_B0, M7_0, N);
    checkError(cudaGetLastError(), "kernel execution", __LINE__);
    
    cudaSetDevice(1);
    kernelM3<<<dimGrid, dimBlock>>>(d_A1, d_B1, M3_1, N);
    kernelM5<<<dimGrid, dimBlock>>>(d_A1, d_B1, M5_1, N);
    cudaEventRecord(X1, 0);
    checkError(cudaGetLastError(), "kernel execution", __LINE__);

    cudaSetDevice(2);
    kernelM2<<<dimGrid, dimBlock>>>(d_A2, d_B2, M2_2, N);
    kernelM4<<<dimGrid, dimBlock>>>(d_A2, d_B2, M4_2, N);
    cudaEventRecord(X2, 0);
    checkError(cudaGetLastError(), "kernel execution", __LINE__);

    cudaSetDevice(3);
    kernelM6<<<dimGrid, dimBlock>>>(d_A3, d_B3, M6_3, N);
    cudaEventRecord(X3, 0);
    checkError(cudaGetLastError(), "kernel execution", __LINE__);

    cudaSetDevice(0);     
    cudaEventSynchronize(X1);
    cudaEventSynchronize(X2);
    cudaEventSynchronize(X3);

    cudaSetDevice(0);
    cudaMemcpyAsync(M1_3, M1_0, numBytes_tmp, cudaMemcpyDeviceToDevice);
    
    cudaSetDevice(1);
    cudaMemcpyAsync(M3_3, M3_1, numBytes_tmp, cudaMemcpyDeviceToDevice);
    cudaMemcpyAsync(M5_0, M5_1, numBytes_tmp, cudaMemcpyDeviceToDevice);
    cudaEventRecord(X1, 0);
    
    cudaSetDevice(2);
    cudaMemcpyAsync(M2_3, M2_2, numBytes_tmp, cudaMemcpyDeviceToDevice);
    cudaMemcpyAsync(M4_0, M4_2, numBytes_tmp, cudaMemcpyDeviceToDevice);
    cudaEventRecord(X2, 0);
    
    cudaSetDevice(0); 
    cudaEventSynchronize(X1);
    cudaEventSynchronize(X2);

    cudaSetDevice(0);
    kernelC11<<<dimGrid, dimBlock>>>(d_C11, N, M1_0, M4_0, M5_0, M7_0);
    checkError(cudaGetLastError(), "kernel execution", __LINE__);

    cudaSetDevice(1);
    kernelC12<<<dimGrid, dimBlock>>>(d_C12, N, M3_1, M5_1);
    checkError(cudaGetLastError(), "kernel execution", __LINE__);
    cudaEventRecord(X1, 0);
        
    cudaSetDevice(2);
    kernelC21<<<dimGrid, dimBlock>>>(d_C21, N, M2_2, M4_2);
    cudaEventRecord(X2, 0);
    checkError(cudaGetLastError(), "kernel execution", __LINE__);

    cudaSetDevice(3);
    kernelC22<<<dimGrid, dimBlock>>>(d_C22, N, M1_3, M2_3, M3_3, M6_3);
    cudaEventRecord(X3, 0);
    checkError(cudaGetLastError(), "kernel execution", __LINE__);

    cudaSetDevice(0);
    cudaEventSynchronize(X1);
    cudaEventSynchronize(X2);
    cudaEventSynchronize(X3);
    cudaEventRecord(E2, 0);
    cudaEventSynchronize(E2);
        
    cudaSetDevice(0);
    cudaMemcpyAsync(h_C11, d_C11, numBytes_tmp, cudaMemcpyDeviceToHost);
    checkError(cudaGetLastError(), "cudaMemcpy result", __LINE__);

    cudaSetDevice(1);
    cudaMemcpy(h_C12, d_C12, numBytes_tmp, cudaMemcpyDeviceToHost);
    checkError(cudaGetLastError(), "cudaMemcpy result", __LINE__);
    cudaEventRecord(X1, 0);

    cudaSetDevice(2);
    cudaMemcpy(h_C21, d_C21, numBytes_tmp, cudaMemcpyDeviceToHost);
    checkError(cudaGetLastError(), "cudaMemcpy result", __LINE__);
    cudaEventRecord(X2, 0);

    cudaSetDevice(3);
    cudaMemcpy(h_C22, d_C22, numBytes_tmp, cudaMemcpyDeviceToHost);
    checkError(cudaGetLastError(), "cudaMemcpy result", __LINE__);
    cudaEventRecord(X3, 0);

    cudaSetDevice(0);
    cudaEventSynchronize(X1);
    cudaEventSynchronize(X2);
    cudaEventSynchronize(X3);
    
    //Free device memory
    cudaSetDevice(0);
    cudaFree(d_A0);
    cudaFree(d_B0);
    cudaFree(d_C11);
    cudaFree(M1_0);
    cudaFree(M4_0);
    cudaFree(M5_0);
    cudaFree(M7_0);
    
    cudaSetDevice(1);
    cudaFree(d_A1);
    cudaFree(d_B1);
    cudaFree(d_C12);
    cudaFree(M3_1);
    cudaFree(M5_1);

    cudaSetDevice(2);
    cudaFree(d_A2);
    cudaFree(d_B2);
    cudaFree(d_C21);
    cudaFree(M2_2);
    cudaFree(M4_2);

    cudaSetDevice(3);
    cudaFree(d_A3);
    cudaFree(d_B3);
    cudaFree(d_C22);
    cudaFree(M1_3);
    cudaFree(M2_3);
    cudaFree(M3_3);
    cudaFree(M6_3);

    cudaSetDevice(0);
    cudaEventRecord(E3, 0);
    cudaEventSynchronize(E3);
    //Check result
    int64_t *h_D = (int64_t *) malloc(N*N*sizeof(int64_t));
    if(testing =='Y'){	
	mult(h_A, h_B, h_D, N);
	merge(h_C11, h_C12, h_C21, h_C22, h_C, N);
	bool result = check(h_D, h_C, N);
	if (result) {
	    printf("Correct result\n");
	}
	else {
	    printf("Wrong result\n");
	    printf("\n\n	GPU 11 result\n");
	    print(h_C11, N>>1);
	    printf("\n\n	GPU 12 result\n");
	    print(h_C12, N>>1);
	    printf("\n\n	GPU 21 result\n");
	    print(h_C21, N>>1);
	    printf("\n\n	GPU 22 result\n");
	    print(h_C22, N>>1);
	    printf("\n\n	Correct result\n");
	    print(h_D, N);
	}
    }
    cudaEventElapsedTime(&TiempoTotal,  E0, E3);
    cudaEventElapsedTime(&TiempoKernel, E1, E2);
    printf("\nSTRASSEN 4 GPU\n");
    printf("Dimensiones: %dx%d\n", N, N);
    printf("nThreads: %dx%d (%d)\n", nThreads, nThreads, nThreads * nThreads);
    printf("nBlocks: %dx%d (%d)\n", nBlocks, nBlocks, nBlocks*nBlocks);
    /*if (PINNED) printf("Usando Pinned Memory\n");
    else printf("NO usa Pinned Memory\n");*/
    printf("Tiempo Global: %4.6f milseg\n", TiempoTotal);
    printf("Tiempo Kernel: %4.6f milseg\n", TiempoKernel);

    //Free host memory
    free(h_A); 
    free(h_B);
    free(h_C);
    free(h_D);
}
