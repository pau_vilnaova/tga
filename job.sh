#!/bin/bash
export PATH=/Soft/cuda/7.5.18/bin:$PATH

### Directivas para el gestor de colas
# Asegurar que el job se ejecuta en el directorio actual
#$ -cwd
# Asegurar que el job mantiene las variables de entorno del shell lamador
#$ -V
# Cambiar el nombre del job
#$ -N MultiGpu
# Cambiar el shell
#$ -S /bin/bash

if [ "$#" -ne 3 ]
then
    echo "Usage: qsub -l cuda job.sh PROGRAM N TEST"
else
    if [ "$1" -eq 1 ]
    then
	nvprof --print-gpu-trace ./strassenCPU.exe $2 $3
    elif [ "$1" -eq 2 ]
    then
	nvprof --print-gpu-trace ./strassenGPU00.exe $2 $3
    elif [ "$1" -eq 3 ]
    then
	nvprof --print-gpu-trace ./strassenGPU.exe $2 $3
    elif [ "$1" -eq 4 ]
    then
	nvprof --print-gpu-trace ./strassen2GPU.exe $2 $3
    elif [ "$1" -eq 5 ]
    then
	nvprof --print-gpu-trace ./strassen2GPU01.exe $2 $3
    elif [ "$1" -eq 6 ]
    then
	nvprof --print-gpu-trace ./strassen4GPU.exe $2 $3
    elif [ "$1" -eq 7 ]
    then
	nvprof --print-gpu-trace ./strassen4GPU01.exe $2 $3
    else
	echo "Program must be a number between 1 and 7"
    fi    
fi


# Para comprobar que funciona no es necesario usar matrices muy grandes
# Con N = 1024 es suficiente
#nvprof ./kernel4GPUs.exe 1024 Y

# Con matrices muy grandes no es recomendable comprobar el resultado
#nvprof --print-gpu-trace ./strassenGPU.exe $1

#nvprof ./strassen2GPU01.exe $1

#nvprof --print-gpu-trace ./kernel4GPUs.exe 4096 N
#./kernel4GPUs.exe 4096 N
#nvprof ./kernel4GPUs.exe 8192 N

